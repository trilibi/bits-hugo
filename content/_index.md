---
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
---

## August 6th - 9th
The 2017 Farm Bureau Actuarial Conference will be held at the Coeur d'Alene Resort in Coeur d'Alene Resort, Idaho.

![The Resort](assets/images/cdaresort-sunset.jpg)