---
date: "2016-05-05T21:48:51-07:00"
title: "Contact"
bigimg: [{src: "assets/images/rainbow.jpg", desc: ""}]

---

**Farm Bureau Mutual Insurance Company of Idaho**  
275 Tierra Vista Dr  
PO Box 4848  
Pocatello ID 83205-4848

**Randy Norquist** FCAS, MAAA  
Actuarial Director  
[208-239-4318](tel:208-239-4318) | fax 208-232-3608  
[rnordquist@idfbins.com](mailto:rnordquist@idfbins.com)

**Jay Call** ACAS, Ph. D  
Associate Actuary  
[208-239-4237](tel:208-239-4237)  
[jcall@idfbins.com](mailto:jcall@idfbins.com)
 

**Stuart Bailey**  
Associate Actuary  
[208-239-4255](tel:208-239-4255)  
[sgbailey@idfbins.com](mailto:sgbailey@idfbins.com)
