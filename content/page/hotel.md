---
date: "2016-05-05T21:48:51-07:00"
title: "Hotel"
stub: hotel
bigimg: [{src: "assets/images/resort/room1.jpg", desc: ""}]
page_title: "The Coeur d'Alene Resort"

---

### [The Coeur d'Alene Resort](https://www.cdaresort.com/)  
115 S. 2nd Street  
Coeur d'Alene, Idaho 83814  
Toll Free: (855) 703-4648